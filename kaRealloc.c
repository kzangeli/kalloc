// 
// FILE            kaRealloc.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include "klog/kLog.h"                  // K Log Lib
#include "kbase/kLibLog.h"              // KLOG_*

#include "kalloc/KaStatus.h"            // KaStatus
#include "kalloc/kaInit.h"              // kaLog - klog component for kalloc lib
#include "kalloc/KaTraceLevel.h"        // Trace Levels for kalloc library
#include "kalloc/kaHooks.h"             // KA_ERROR_HOOK
#include "kalloc/KAlloc.h"              // KAlloc
#include "kalloc/kaAlloc.h"             // kaAlloc
#include "kalloc/kaRealloc.h"           // Own Interface



// -----------------------------------------------------------------------------
//
// kaRealloc -
//
char* kaRealloc(KAlloc* kaP, char* origBuf, unsigned long long size)
{
  KLOG_T(KatAlloc, "%s re-allocating %d byte for %s:%s", kaP->name, size, what, info);

  char* buf = kaAlloc(kaP, size);

  if (buf == NULL)
  {
    KA_ERROR_HOOK(kaP, KasAllocError, "Unable to allocate buffer", NULL);
    return NULL;
  }

  strcpy(buf, origBuf);
  return buf;
}
