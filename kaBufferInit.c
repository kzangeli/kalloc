// 
// FILE            kaBufferInit.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include <stdio.h>                      // NULL
#include <string.h>                     // strdup
#include <strings.h>                    // bzero

#include "kbase/kLibLog.h"              // KLOG_*
#include "kalloc/KaTraceLevel.h"        // KaTraceLevel
#include "kalloc/kaInit.h"              // kaLog - klog component for kalloc library
#include "kalloc/KAlloc.h"              // KAlloc
#include "kalloc/kaHooks.h"             // KaErrorHook
#include "kalloc/kaBufferInit.h"        // Own interface



// -----------------------------------------------------------------------------
//
// kaBufferInit -
//
void kaBufferInit
(
  KAlloc*      kaP,           // pointer to kalloc buffer
  char*        buf,           // the initial buffer to use for allocations
  int          bufSize,       // the size of the initial buffer
  int          allocSize,     // the size to use for subsequent allocations, when the init-buffer runs out
  KaErrorHook  errorHook,     // function pointer to be called when errors occur
  const char*  name           // name of the buffer
)
{
  // sem_init(&kaP->sem, 0, 1);

  kaP->initBuf       = buf;
  kaP->initBufSize   = bufSize;
  kaP->allocations   = 0;
  kaP->allocPointer  = buf;
  kaP->bytesLeft     = bufSize;
  kaP->errorHook     = errorHook;
  kaP->allocSize     = allocSize;
  kaP->allocList     = NULL;
  kaP->name          = (char*) name;

  KLOG_T(KatInit, "Initialized buffer at %p, allocSize is %d", kaP->initBuf, kaP->allocSize);
}
