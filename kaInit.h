// 
// FILE            kaInit.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#ifndef KALLOC_KAINIT_H_
#define KALLOC_KAINIT_H_

#include "klog/KlComponent.h"           // KlComponent

#include "kalloc/KaStatus.h"            // KaStatus
#include "kalloc/kaLog.h"               // KaLogFunction



// -----------------------------------------------------------------------------
//
// kaLog - klog component for kalloc library
//
extern KlComponent*  kaLog;



// -----------------------------------------------------------------------------
//
// kaInit - initialize the kalloc library
//
extern KaStatus kaInit(KaLogFunction logFunction);

#endif  // KALLOC_KAINIT_H_
