#ifndef KALLOC_KALOG_H_
#define KALLOC_KALOG_H_

// 
// FILE            kaLog.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 



// -----------------------------------------------------------------------------
//
// KaLogFunction - 
//
typedef void (*KaLogFunction)
(
  int          severity,              // 1: Error, 2: Warning, 3: Info, 4: Msg, 5: Verbose, 6: Trace
  int          level,                 // Trace level || Error code || Info Code
  const char*  fileName,
  int          lineNo,
  const char*  functionName,
  const char*  format,
  ...
);



#ifdef KA_LOG_ON
// -----------------------------------------------------------------------------
//
// KAL_E
//
#define KAL_E(...) kaLogFunction(1, 0, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)



// -----------------------------------------------------------------------------
//
// KAL_RE
//
#define KAL_RE(retVal, ...) do { kaLogFunction(1, 0, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__); return retVal; } while (0)



// -----------------------------------------------------------------------------
//
// KAL_W
//
#define KAL_W(...) kaLogFunction(2, 0, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)



// -----------------------------------------------------------------------------
//
// KAL_I
//
#define KAL_I(...) kaLogFunction(3, 0, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)



// -----------------------------------------------------------------------------
//
// KAL_M
//
#define KAL_M(...) kaLogFunction(4, 0, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)



// -----------------------------------------------------------------------------
//
// KAL_V
//
#define KAL_V(...) kaLogFunction(5, 0, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)



// -----------------------------------------------------------------------------
//
// KAL_T
//
#define KAL_T(tLevel, ...) kaLogFunction(6, tLevel, __FILE__, __LINE__, __FUNCTION__, __VA_ARGS__)



#else

#define KAL_E(...)
#define KAL_RE(retVal, ...) return retVal
#define KAL_W(...)
#define KAL_I(...)
#define KAL_M(...)
#define KAL_V(...)
#define KAL_T(tLevel, ...)
#endif



// -----------------------------------------------------------------------------
//
// kaLogFunction - 
//
extern KaLogFunction kaLogFunction;

#endif  // KALLOC_KA_LOG_H_
