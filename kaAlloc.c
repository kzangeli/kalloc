// 
// FILE            kaAlloc.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include "kbase/kLibLog.h"              // KLOG_*
#include "kalloc/KaStatus.h"            // KaStatus
#include "kalloc/kaInit.h"              // kaLog - klog component for kalloc lib
#include "kalloc/KaTraceLevel.h"        // Trace Levels for kalloc library
#include "kalloc/kaHooks.h"             // KA_ERROR_HOOK
#include "kalloc/KAlloc.h"              // KAlloc
#include "kalloc/kaLog.h"               // KAL_*
#include "kalloc/kaAlloc.h"             // Own Interface



// ----------------------------------------------------------------------------- 
//
// kaAlloc - 
//
char* kaAlloc(KAlloc* kaP, unsigned long long size)
{
  if (size >= kaP->allocSize - sizeof(KaAllocBuffer))
  {
    KLOG_E("KALL: ********************************** TOO BIG (%d bytes - max size if %d)", size, kaP->allocSize - sizeof(KaAllocBuffer));
    return NULL;
  }

  // sem_wait(&kaP->sem);

  //
  // Take a chunk from the allocation buffer (kaP->allocPointer)
  //
  if (kaP->bytesLeft > size)
  {
    char* start = kaP->allocPointer;  // As kaP->allocPointer is set to NEXT chunk in the next line

    // KLOG_M("KALL: allocPointer before: %p", kaP->allocPointer); 
    kaP->allocPointer   += size;
    kaP->bytesLeft      -= size;
    // KLOG_M("KALL: allocPointer after: %p", kaP->allocPointer); 

    // KLOG_T(KatAllocBytesLeft, "returning a buffer of %d bytes at %p", size, start);
    // sem_post(&kaP->sem);
    return start;
  }

  // KLOG_M("KALL: ALLOCATING ADDITIONAL BUFFER of %d bytes (using calloc)", kaP->allocSize);
  kaP->allocPointer = (char*) calloc(1, kaP->allocSize);
  if (kaP->allocPointer == NULL)
  {
    KLOG_M("KALL: ******************************* ERROR - out of memory: calloc returned NULL !!!");
    KA_ERROR_HOOK(kaP, KasAllocError, "Unable to allocate buffer", NULL);
    // sem_post(&kaP->sem);
    return NULL;
  }
  kaP->bytesLeft = kaP->allocSize;
  // KLOG_M("KALL: right after allocating more buffer: %d bytes left", kaP->bytesLeft);

  //
  // Save pointer to allocated buffer in kaP->allocList
  //
  // [ First, room for KaAllocBuffer is allocated in the beginning of the newly allocated buffer ]
  //
  KaAllocBuffer* newKabP = (KaAllocBuffer*) kaP->allocPointer;

  kaP->allocPointer += sizeof(KaAllocBuffer);
  kaP->bytesLeft    -= sizeof(KaAllocBuffer);

  if (kaP->allocList == NULL)
    kaP->allocList = newKabP;
  else
  {
    KaAllocBuffer* kabP = kaP->allocList;

    while (kabP->next != NULL)
      kabP = kabP->next;
    kabP->next = newKabP;
  }

  newKabP->toFree = (char*) newKabP;
  newKabP->next   = NULL;
  
  char* start = kaP->allocPointer;  // As kaP->allocPointer is set to NEXT chunk in the next line
  
  // Now, position the allocPointer for the next call ...
  kaP->allocPointer   += size;

  // ... and count off the size of the chunk just given away
  kaP->bytesLeft      -= size;

  // KLOG_M("KALL: end-of-function: returning a buf od %d bytes at %p, and bytesLeft: %d", size, start, kaP->bytesLeft);

  // sem_post(&kaP->sem);
  return start;
}
