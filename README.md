# kalloc - faster allocation of temporary buffers

## Table of Contents

## Background
`malloc` is a very slow function and I decided to implement my own, *fast* alloction facility.
This library maintains a large buffer and then gives out small chunks of this buffer.
When the large buffer finishes, another large buffer is allocated and then portioned out.
There is no way to `free` single allocations, as no tree of allocated chunks is maintained.
The "large buffer" is simply portioned out in order and when finished, another one is allocated and used.

This approach is not very good for memory poor environments but extremely good for performance.
The library was implemented to aid REST servers, which by nature benefit from this way of working.


## Install
% make di  # debug install

## Usage
```
#include "kbase/kTypes.h"                     // KFALSE
#include "kalloc/kaBufferInit.h"              // kaBufferInit
#include "kalloc/kaAlloc.h"                   // kaAlloc
#include "kalloc/kaBufferReset.h"             // kaBufferReset

int main()
{
  KAlloc kalloc;
  char   initBuffer[1024];

  // Initialize the kalloc lib, with an initial buffer and a subsequent allocation size of 2k
  kaBufferInit(&kalloc, initBuffer, sizeof(initBuffer), 2048, NULL, "main");

  // Allocate a buffer of 44 bytes
  char* cBuf = kaAlloc(&kalloc, 44);

  ...

  kaBufferReset(&kalloc, KFALSE);
}
```

There is also a string duplication function `kaStrdup(KAlloc* kaP, const char* s)`. that allocates from the and KAlloc buffer,
and a `kaRealloc(KAlloc* kaP, char* origBuf, unsigned long long size)` to do what `realloc` does, but reallocating on the KAlloc buffer.

## License
[Apache 2.0](LICENSE) © 2017-2020 Ken Zangelin
