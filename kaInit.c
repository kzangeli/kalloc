// 
// FILE            kaInit.c
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#include "kbase/kTypes.h"               // KBool

#include "klog/kLog.h"                  // K Log macros
#include "klog/KlComponent.h"           // KlComponent
#include "klog/klComponentRegister.h"   // klComponentRegister

#include "kalloc/KaStatus.h"            // KaStatus
#include "kalloc/kaLog.h"               // KaLogFunction
#include "kalloc/KaTraceLevel.h"        // KaTraceLevel


#ifdef USE_KL_LOG
// -----------------------------------------------------------------------------
//
// traceLevelInfo
//
static KlComponentTraceLevelInfo traceLevelInfo[] =
{
  { KatInit,           "Init"                        },
  { KatNewBuffer,      "New Allocated Buffer"        },
  { KatAllocBytesLeft, "Bytes left in alloc buffer"  },
  { KatAlloc,          "Individual Allocations"      }
};



// -----------------------------------------------------------------------------
//
// kaLog - klog component for kalloc library
//
KlComponent*  kaLog       = NULL;
#endif

static KBool  initialized = KFALSE;



// -----------------------------------------------------------------------------
//
// kaInit - initialize the kalloc library
//
KaStatus kaInit(KaLogFunction logFunction)
{
  if (initialized == KTRUE)
    return KasAlreadyInitialized;

#ifdef USE_KL_LOG
  //
  // First of all, create a klog component
  //
  kaLog = klComponentRegister("kalloc", KatLast, traceLevelInfo);
  if (kaLog == NULL)
    KBL_RE(KasLogComponentError, ("klComponentRegister error"));

#endif



  //
  // Assign the "library log function", so that the logs are tied to the executable that the library is part of
  //
  kaLogFunction = logFunction;

  initialized = KTRUE;
  return KasOk;
}
