// 
// FILE            kaBufferInit.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#ifndef KALLOC_KABUFFERINIT__H_
#define KALLOC_KABUFFERINIT__H_

#include "kalloc/KaStatus.h"            // KaStatus
#include "kalloc/KAlloc.h"              // KAlloc
#include "kalloc/kaHooks.h"             // KaErrorHook



// -----------------------------------------------------------------------------
//
// kaBufferInit -
//
extern void kaBufferInit
(
  KAlloc*      kaP,           // pointer to kalloc buffer
  char*        buf,           // the initial buffer to use for allocations
  int          bufSize,       // the size of the initial buffer
  int          allocSize,     // the size to use for subsequent allocations, when the init-buffer runs out
  KaErrorHook  errorHook,     // function pointer to be called when errors occur
  const char*  name           // name of the buffer
);

#endif  // KALLOC_KABUFFERINIT__H_
