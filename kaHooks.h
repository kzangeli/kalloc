// 
// FILE            kaHooks.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// 
#ifndef KALLOC_KAHOOKS_H_
#define KALLOC_KAHOOKS_H_

#include "kalloc/KaStatus.h"            // KaStatus



// -----------------------------------------------------------------------------
//
// Forward declaration of KAlloc
//
// NOTE
//   KAlloc contains a KaErrorHook, so KAlloc.h cannot be included here.
//
struct KAlloc;


// -----------------------------------------------------------------------------
//
// KA_ERROR_HOOK
//
#define KA_ERROR_HOOK(kaP, kas, errorString, vP)              \
do                                                            \
{                                                             \
  if (kaP->errorHook != NULL)                                 \
    kaP->errorHook(kaP, kas, errorString, vP);                \
} while (0)



// ----------------------------------------------------------------------------- 
//
// KaErrorHook - function type for Error Hook callbacks
//
typedef void (*KaErrorHook)(struct KAlloc* kaP, KaStatus kas, char* errorString, void* vP);

#endif  // KALLOC_KAHOOKS_H_
