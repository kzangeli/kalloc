#ifndef KALLOC_KALLOC_H_
#define KALLOC_KALLOC_H_

// 
// FILE            KAlloc.h
// 
// AUTHOR          Ken Zangelin
// 
// Copyright 2019 Ken Zangelin
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with theLicense.
// You may obtain a copy of the License at
// 
//     http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//
#include <semaphore.h>                  // sem_t

#include "kalloc/kaHooks.h"             // KaErrorHook
#include "kalloc/KaAllocBuffer.h"       // KaAllocBuffer



// ----------------------------------------------------------------------------- 
//
// KAlloc -
//
typedef struct KAlloc
{
  sem_t           sem;            // semaphore to protect the allocation buffer
  char*           initBuf;        // pointer to the initial buffer
  int             initBufSize;    // total size of initial buffer
  int             allocations;    // number of additional allocations done 
  char*           allocPointer;   // pointer to next free byte
  unsigned int    bytesLeft;      // remaining bytes in current alloc buffer
  KaErrorHook     errorHook;      // points to a function that is to be called on errors

  int             allocSize;      // size of new new allocations
  KaAllocBuffer*  allocList;      // linked list of allocated buffers - to be freed

  char*           name;           // For debugging purposes only
} KAlloc;

#endif  //  KALLOC_KALLOC_H_
