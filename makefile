# 
# FILE            makefile
# 
# AUTHOR          Ken Zangelin
# 
# Copyright 2019 Ken Zangelin
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with theLicense.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# 
LIB_SO        = libkalloc.so
LIB           = libkalloc.a
CC            = gcc
CCCC          = g++
INCLUDE       = -I..
DFLAGS        = -DANSI # -DKL_ON -DKBLOG_ON -DKA_LOG_ON
CFLAGS        = -O2 -Wall -fPIC -Wno-unused-function -fstack-protector-all $(DFLAGS) $(INCLUDE)
LIB_SOURCES   = KaStatus.c      \
                kaAlloc.c       \
                kaRealloc.c     \
                kaStrdup.c      \
                kaBufferInit.c  \
                kaBufferReset.c \
                kaInit.c        \
                kallocVersion.c \
                kaLog.c

LIB_OBJS      = $(LIB_SOURCES:c=o)

TEST          = kallocTest
TEST_SOURCES  = kallocTest.c
TEST_OBJS     = $(TEST_SOURCES:c=o)

LIBS          = ../klog/libklog.a ../kbase/libkbase.a -lpthread -lrt

all: $(LIB_SO) $(LIB) $(TEST)

clean:
						rm -f *.o
						rm -f *.a
						rm -f *~
						rm -f *.so
						rm -f $(TEST)

install:    all
						@if [ ! -d bin ]; then mkdir bin; fi
						cp $(TEST) bin/

di:         install

ci:         clean install

$(LIB):			$(LIB_OBJS) $(LIB_SOURCES)
						ar r $(LIB) $(LIB_OBJS)
						ranlib $(LIB)

$(LIB_SO):	$(LIB_OBJS) $(LIB_SOURCES)
						$(CC) -shared $(LIB_OBJS) -o $(LIB_SO)

$(TEST):		$(TEST_OBJS) $(LIB)
						$(CCCC) -o $(TEST) $(TEST_OBJS) $(LIB) $(LIBS)


%.o: %.c
						$(CC) $(CFLAGS) -c $^ -o $@

%.o: %.cpp
						$(CCCC) $(CFLAGS) -c $^ -o $@

%.i: %.c
						$(CC) $(CFLAGS) -c $^ -E > $@

%.i: %.cpp
						$(CCCC) $(CFLAGS) -c $^ -E > $@

%.cs: %.c
						$(CC) -Wa,-adhln -g $(CFLAGS)  $^  > $@
